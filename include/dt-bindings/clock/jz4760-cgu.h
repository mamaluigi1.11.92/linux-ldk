/* SPDX-License-Identifier: GPL-2.0 */
/*
 * This header provides clock numbers for the ingenic,jz4760-cgu DT binding.
 */

#ifndef __DT_BINDINGS_CLOCK_JZ4760_CGU_H__
#define __DT_BINDINGS_CLOCK_JZ4760_CGU_H__

#define JZ4760_CLK_EXT      0
#define JZ4760_CLK_OSC32K   1
#define JZ4760_CLK_PLL0     2
#define JZ4760_CLK_PLL1     3
#define JZ4760_CLK_CCLK     4
#define JZ4760_CLK_HCLK     5
#define JZ4760_CLK_H2CLK    6
#define JZ4760_CLK_PCLK     7
#define JZ4760_CLK_MCLK     8
#define JZ4760_CLK_SCLK     9
#define JZ4760_CLK_CIM      10
#define JZ4760_CLK_LPCLK_MUX    11
#define JZ4760_CLK_MMC_MUX  12
#define JZ4760_CLK_UHC      13
#define JZ4760_CLK_GPU      14
#define JZ4760_CLK_GPS      15
#define JZ4760_CLK_SSI_MUX  16
#define JZ4760_CLK_PCM_MUX  17
#define JZ4760_CLK_I2S      18
#define JZ4760_CLK_OTG      19
#define JZ4760_CLK_SSI0     20
#define JZ4760_CLK_SSI1     21
#define JZ4760_CLK_SSI2     22
#define JZ4760_CLK_PCM      23
#define JZ4760_CLK_DMA      24
#define JZ4760_CLK_I2C0     25
#define JZ4760_CLK_I2C1     26
#define JZ4760_CLK_UART0    27
#define JZ4760_CLK_UART1    28
#define JZ4760_CLK_UART2    29
#define JZ4760_CLK_UART3    30
#define JZ4760_CLK_IPU      31
#define JZ4760_CLK_ADC      32
#define JZ4760_CLK_AIC      33
#define JZ4760_CLK_VPU      34
#define JZ4760_CLK_MMC0     35
#define JZ4760_CLK_MMC1     36
#define JZ4760_CLK_MMC2     37
#define JZ4760_CLK_OTG_PHY  38
#define JZ4760_CLK_UHC_PHY  39
#define JZ4760_CLK_EXT512   40
#define JZ4760_CLK_RTC      41

#endif /* __DT_BINDINGS_CLOCK_JZ4760_CGU_H__ */
